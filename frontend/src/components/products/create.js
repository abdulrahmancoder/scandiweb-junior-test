import {Col, Container, Row, Button, Form, Table} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";
import {useState} from "react";
import { useHistory} from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function NewProduct() {

    let history=useHistory();

    const [sku, setSku] = useState(null)
    const [name, setName] = useState(null)
    const [price, setPrice] = useState(null)
    const [width, setWidth] = useState(null)
    const [height, setHeight] = useState(null)
    const [length, setLength] = useState(null)
    const [size, setSize] = useState(null)
    const [weight, setWeight] = useState(null)
    const [product_type_id, setProduct_type_id] = useState(null)
    const [Type, setType] = useState(0)
    const [hint, setHint]=useState("")

    function switcher(event) {
        setType(event.target.value)
        setProduct_type_id(event.target.value)
        if (event.target.value==3)
            setHint('please provide the dimensions of furniture in CM')
        if (event.target.value==1)
            setHint('please provide Disc space in MB')
        if (event.target.value==2)
            setHint('please provide the the book weight in KG')
    }

    const notifyMissingData = () => {
        toast.error("Please, submit required data", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            }
        );
    }

    const notifyInvalidData = () => {
        toast.error("Please, provide the data of indicated type", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });

    }

    function hasNumber(str) {
        return /\d/.test(str);
    }

    function validation(){
        if (typeof (sku)=='number' || hasNumber(name)){
            return -1;
        }
        if(sku==null || name ==null || price ==null || product_type_id == null ){
            return 0;
        }
        else {

            if (product_type_id == '1') {
                if (size == null) {
                    return 0;
                }
            }
            else if (product_type_id == '2') {
                if (weight == null) {
                    return 0;
                }
            }
            else if (product_type_id == '3') {
                if (width == null || height == null || length == null) {
                    return 0;
                }
            }

        }

        return 1;
    }

    function save() {
        if (validation()===0){
            notifyMissingData()
        }
        else if (validation()===-1){
            notifyInvalidData()
        }
        else {
            let headers = new Headers();
            headers.append('Access-Control-Allow-Origin', '*');
            headers.append('Access-Control-Allow-Credentials', 'true');
            headers.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');


            let form = new FormData();
            form.append('sku', sku.toString());
            form.append('name', name.toString());
            form.append('price', price.toString());
            if (size !== null)
                form.append('size', size.toString());
            if (weight !== null)
                form.append('weight', weight.toString());

            if (width !== null)
                form.append('width', width.toString());

            if (height !== null)
                form.append('height', height.toString());

            if (length !== null)
                form.append('length', length.toString());

            if (product_type_id !== null)
                form.append('product_type_id', product_type_id.toString());

            axios.post('https://scandi-test.000webhostapp.com/product/create',

                form

                ,
                {
                    headers: headers,
                }
            )
                .then(function (response) {

                    history.push('/')
                })
                .catch(function (error) {

                    console.log(error);
                })
        }
    }

    function redirectHome(event) {
        history.push('/')
    }


    return (
        <Container>
            <ToastContainer />
            <br/>
            <Row>
                <Col>
                    <h1>Product Add</h1>
                </Col>
                <Col> </Col>
                <Col> </Col>
                <Col>
                    <Button onClick={event => save(event)} className='m-3' variant={'primary'}>Save</Button>

                    <Button onClick={event => redirectHome(event)} variant={'warning'}>Cancel</Button>
                </Col>
            </Row>
            <hr/>
            <Row>
                <Col xs={5}>
                    <Form id='product_form'>
                        <Table>
                            <tr>
                                <td><h5>SKU</h5></td>
                                <td><Form.Control key={'sku'} id={'sku'}
                                                  onChange={event => setSku(event.target.value)}
                                                  type="text" placeholder="#SKU"/></td>
                            </tr>
                            <br/>
                            <tr>
                                <td><h5>Name</h5></td>
                                <td><Form.Control key={'name'} id={'name'}
                                                  onChange={event => setName(event.target.value)}
                                                  type="text" placeholder="#Name"/></td>
                            </tr>
                            <br/>
                            <tr>
                                <td><h5>Price $</h5></td>
                                <td><Form.Control key={'price'} id={'price'}
                                                  onChange={event => setPrice(event.target.value)}
                                                  type="number" placeholder="#Price"/></td>
                            </tr>
                            <br/>
                            <tr>
                                <td><h5>Switcher Type</h5></td>
                                <td><Form.Select id={'productType'}
                                                 onChange={event => switcher(event)} aria-label="Default select example">
                                    <option>Type Switcher</option>
                                    <option value="1">DVD</option>
                                    <option value="2">Book</option>
                                    <option value="3">Furniture</option>
                                </Form.Select></td>
                            </tr>
                            <br/>
                            <tr><td colSpan={2}><b>{hint}</b></td></tr>

                            {
                                Type == 3 ?
                                    <>
                                        <tr>
                                            <td><h5>Height (CM)</h5></td>
                                            <td><Form.Control key={'height'} id={'height'}
                                                              onChange={event => setHeight(event.target.value)}
                                                              type="number" placeholder="#Height"/></td>
                                        </tr>
                                        <br/>
                                        <tr>
                                            <td><h5>Width (CM)</h5></td>
                                            <td><Form.Control key={'width'} id={'width'}
                                                              onChange={event => setWidth(event.target.value)}
                                                              type="number" placeholder="#Width"/></td>
                                        </tr>
                                        <br/>
                                        <tr>
                                            <td><h5>Length (CM)</h5></td>
                                            <td><Form.Control key={'length'} id={'length'}
                                                              onChange={event => setLength(event.target.value)}
                                                              type="number" placeholder="#Length"/></td>
                                        </tr>
                                    </> :
                                    Type == 1 ?
                                        <>

                                            <tr>
                                                <td><h5>Size (MB)</h5></td>
                                                <td><Form.Control key={'size'} id={'size'}
                                                                  onChange={event => setSize(event.target.value)}
                                                                  type="number" placeholder="#Size"/></td>
                                            </tr>
                                        </> :
                                        Type == 2 ?
                                            <>

                                                <tr>
                                                    <td><h5>Weight (KG)</h5></td>
                                                    <td><Form.Control key={'weight'} id={'weight'}
                                                                      onChange={event => setWeight(event.target.value)}
                                                                      type="number" placeholder="#Weight"/></td>
                                                </tr>
                                            </> : <></>
                            }
                        </Table>
                    </Form>
                </Col>
            </Row>
            <br/><br/>
        </Container>
    )
}