import {Col, Container, Row, Button, FormCheck} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";
import {useEffect, useState} from "react";
import {Link} from "react-router-dom";

export default function Index(){
    
    const [products, setProducts]=useState([])
    const [checkedItems, setCheckedItems]=useState([])
    const [loading,setLoading]=useState(false)

    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('Access-Control-Allow-Origin', "*");
    headers.append('Access-Control-Allow-Credentials', 'true');
    headers.append('Access-Control-Allow-Methods','GET,PUT,POST,DELETE,PATCH,OPTIONS');

    useEffect(() => {
        getAllProducts()

    },[])

    async function getAllProducts() {
        setLoading(true)
        const response = await fetch(
            `https://scandi-test.000webhostapp.com/product/getAll`
        );
        const data = await response.json();
        setLoading(false)
        setProducts(data);
        // console.log(data)

    }
    function check(event){
        let id=parseInt(event.target.value)
        if (event.target.checked===true) {
            setCheckedItems(checkedItems => [
                ...checkedItems, id.toString()
            ]);
        }
        else {
            const filterItems=checkedItems.filter(item=>{
                return item.toString() !==id.toString();
            })
            setCheckedItems(filterItems);
        }
    }
    function massDelete(event){


        let form=new FormData();
        form.append('checkedProducts',JSON.stringify(checkedItems));

            axios.post('https://scandi-test.000webhostapp.com/product/massDelete',
                form
        ,
                {
                    headers: headers,
                }
                )
                .then(function (response) {
                    
                   getAllProducts();

                    console.log(response.data);
                })
                .catch(function (error) {

                    console.log(error);
                })
    }

    return(

       <Container>
           <br/>
           <Row>
               <Col>
                   <h1>Products List</h1>
               </Col>
               <Col> </Col>
               <Col> </Col>
               <Col>
                   <Link to='/addProduct'> <Button className='m-3' variant={'primary'}>ADD</Button></Link>

                  <Button onClick={e=>massDelete(e)} variant={'warning'}>MASS DELETE</Button>
               </Col>
           </Row>


                   <Row>
                   {
                       products.map((i, index) => {
                           return (
                               <Col xs={3} key={i.id.toString()}>
                                   <div className='align-content-center product-item'>
                                       <div className='form-check'>
                                   <span className='pull-left'>
                                    <FormCheck className={'delete-checkbox'} value={i.id}
                                               onChange={event => check(event)}>

                                    </FormCheck>
                                   </span>
                                       </div>
                                       <p>{i.sku}</p>
                                       <p>{i.name}</p>
                                       <p>{i.price} $</p>
                                       <p>{
                                           i.size != null ? "size: " + i.size + " MB" :
                                               i.weight != null ? "weight: " + i.weight + " KG" :
                                                   "Dimensions: " + i.width + "x" + i.height + "x" + i.length
                                       }</p>
                                   </div>
                               </Col>
                           )
                       })

                   }
                   </Row>
           <br/><br/>

       </Container>
    )
}
