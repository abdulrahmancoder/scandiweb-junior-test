import {Col,Container, Row, Button, Form, Table} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';



export default function footer() {


    return (
        <Container fluid className='justify-content-center' id={'mainFooter'}>
        <Row>
            <hr/>
            <p className="text-center">Scandi-Web Test assignment</p>
        </Row>
        </Container>
    )
}