
import './App.css';
import axios from "axios";
import {BrowserRouter as Router, Switch, Route, Link, useHistory} from 'react-router-dom';
import index from './components/products/index'
import NewProduct from "./components/products/create";
import Footer from "./components/footer";
function App() {


  return (
    <div className="App">
        <Router>
            <Switch>
                <Route exact path='/' component={index} />
                <Route exact path='/addproduct' component={NewProduct} />
            </Switch>
            <Footer/>

        </Router>
    </div>
    
  );
}

export default App;
