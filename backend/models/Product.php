<?php

namespace Models;


use Core\abstractProduct;

class Product extends abstractProduct
{


    public function __construct()
    {
        parent::__construct();
        $this->conn = $this->getConn();
        $arguments = func_get_args();
        $numberOfArguments = func_num_args();

        if (method_exists($this, $function = '__construct' . $numberOfArguments)) {
            call_user_func_array(array($this, $function), $arguments);
        }
    }


    public function __construct1($ass_arr)
    {
        $this->ass_arr = $ass_arr;
        $this->setName($ass_arr['name']);
        $this->setSKU($ass_arr['sku']);
        $this->setPrice($ass_arr['price']);
        $this->setProduct_type_id($ass_arr['product_type_id']);
        $this->setType($this->getProduct_type_id());

    }

    public function __construct2(){}

    private function getProductTypes(): array
    {
        $types = parent::getTypes();
        $newArrTypes = [];
        foreach ($types as $type) {
            $newArrTypes += [$type->id => $type->name];
        }
        return $newArrTypes;
    }

    public static function getAll()
    {
        return parent::All();
    }


    public function setType($typeId)
    {

        $types = $this->getProductTypes();
        $type = $types[$typeId];
        $className = "ProductTypes\\" . $type;
        $this->type = $className;
    }


    public function save(): int
    {
        $productType = $this->type;
        $obj = new $productType($this->ass_arr);
        $vars = $obj->getParams() + $this->sharedParams();
        $stmt = $this->conn->query($obj->getStoreQuery());
        foreach ($vars as $key => &$value) {

            $stmt->bindValue(':' . $key, strval($value));

        }
        if ($this->conn->execute()) {
            return 1;
        } else
            return 0;
    }

//    public function find($id){
//        $q="Select * from product
//            where id=:id";
//        $stmt=($this->getConn())->query($q);
//        $stmt->bindValue(':id',$id);
//        $res=$this->conn->single();
//        return new $this((array)$res);
//
//    }


    public function delete($id)
    {

        $sql = "DELETE FROM product
        WHERE id =:id";
        $this->conn = parent::getConn();
        $this->conn->query($sql);
        $this->conn->bind(':id', $id);
        return $this->conn->execute();
    }


    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getProduct_type_id()
    {
        return $this->product_type_id;
    }

    /**
     * @param mixed $product_type_id
     */
    public function setProduct_type_id($product_type_id)
    {
        $this->product_type_id = $product_type_id;
    }

}