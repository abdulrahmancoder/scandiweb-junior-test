<?php
namespace Models;


use Core\Database;
use PDO;
use PDOException;

class Model
{
    private $conn;
    private static $con;

    public function __construct(){
        $this->conn = new Database();
    }
    protected static function selfCon(){
        return self::$con=new Database();
    }

    /**
     * @return Database
     */
    protected function getConn()
    {
        return $this->conn;
    }


}