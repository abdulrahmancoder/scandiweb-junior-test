<?php


namespace Core;


interface ProductInterface
{
    function getStoreQuery();
    function getParams();
}