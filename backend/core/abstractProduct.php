<?php


namespace Core;

use Models\Model;

abstract class abstractProduct extends Model
{

    protected $sku;
    protected $name;
    protected $price;
    protected $product_type_id;
    protected $type;
    protected $ass_arr;
    protected $conn;

     protected static function All(){
        $con=parent::selfCon();
        $con->query("SELECT * FROM product");
        return $con->resultSet();
    }

    protected static function getTypes(){
        $con=parent::selfCon();
        $con->query("SELECT * FROM product_type");
        return $con->resultSet();
    }

    abstract function delete($id);
    abstract function save();

}