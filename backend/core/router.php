<?php

namespace Core;

class Router
{
    private $controller = 'controllers\ProductController';
    private $method     = 'index';
    private $params     = [];


    public function __construct()
    {
        $this->parseUri();

    }


    private function parseUri()
    {
        $uri = $_SERVER['REQUEST_URI'] ?? '';


        $uri = explode('/', trim(strtolower($uri), '/'));

        // controller
        if (!empty($uri[2])) {

            $controller = $uri[2] . 'Controller';
            unset($uri[2]);
            $controller = 'Controllers\\' . $controller;

            if (class_exists($controller)) {
                $this->controller = $controller;
            } else {
                exit('404');
            }
        }


        $class = $this->controller;
        $class = new $class;

        // method
        if (isset($uri[3])) {

            $method = $uri[3];
            unset($uri[3]);

            if (method_exists($class, $method)) {
                $this->method = $method;
            } else {
                exit();
            }
        }

        // params
        if (isset($uri[4])) {
            $this->params = array_values($uri);
        }

        call_user_func_array([$class, $this->method], $this->params);
    }
}

