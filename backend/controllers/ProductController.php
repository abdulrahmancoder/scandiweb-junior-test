<?php

namespace Controllers;
use Models\Product;
class ProductController
{

    public function index(){
        echo '<center><h1>hello nice to meet you</h1></center><br> path is'. $_SERVER['REQUEST_URI'];
    }

    public function create(){
            $product=new Product($_POST);
            $product->save();
    }

    public function getAll(){
        header('Access-Control-Allow-Origin: *');
        $products=Product::getAll();
        echo json_encode($products);
    }


    private function delete($id){

        $obj=new Product();
        $obj->delete($id);

    }

    public function massDelete(){
        $postedData = $_POST["checkedProducts"];
        $tempData = str_replace("\\", "",$postedData);
        $data = json_decode($tempData);
        foreach ($data as $id){
            $this->delete($id);
        }

    }

}