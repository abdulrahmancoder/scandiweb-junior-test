<?php
namespace ProductTypes;
use Core\ProductInterface;
use Models\Product;
class DVD_desk extends Product implements ProductInterface
{

    private $size;

    public function __construct($ass_arr)
    {
        parent::__construct($ass_arr);
        $this->setSize( $ass_arr['size']);

    }

    function getStoreQuery()
    {
        return "INSERT INTO product 
            (sku,name,price,size,product_type_id)
            VALUES(:sku,:name,:price,:size,:product_type_id)";
    }

    function getParams()
    {
        return [

            'size'=>$this->getSize(),
        ];
    }



    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }


}