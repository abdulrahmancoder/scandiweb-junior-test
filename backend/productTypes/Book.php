<?php
namespace ProductTypes;
use Core\ProductInterface;
use Models\Product;

class Book extends Product implements ProductInterface
{
    private $weight;

    public function __construct($ass_arr)
    {
        parent::__construct($ass_arr);
        $this->setWeight( $ass_arr['weight']);

    }

    function getStoreQuery()
    {
        return "INSERT INTO product 
            (sku,name,price,weight,product_type_id)
            VALUES(:sku,:name,:price,:weight,:product_type_id)";
    }

    function getParams()
    {
        return [
          'weight'=>$this->getWeight(),
        ];
    }
    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

}