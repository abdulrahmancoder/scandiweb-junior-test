<?php
namespace ProductTypes;
use Core\ProductInterface;
use Models\Product;

class Furniture extends Product implements ProductInterface
{
    private $width;
    private $height;
    private $length;

    public function __construct($ass_arr)
    {
        parent::__construct($ass_arr);
        $this->setHeight( $ass_arr['height']);
        $this->setLength( $ass_arr['length']);
        $this->setWidth($ass_arr['width']);
    }

    function getStoreQuery()
    {
        return "INSERT INTO product 
            (sku,name,price,height,width,length,product_type_id)
            VALUES(:sku,:name,:price,:width,:height,:length,:product_type_id)";
    }

    function getParams()
    {
        return [

            'width'=>$this->getWidth(),
            'height'=>$this->getHeight(),
            'length'=>$this->getLength(),

        ];
    }
    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * Furniture constructor.
     * @param $width
     * @param $height
     * @param $length
     */

}