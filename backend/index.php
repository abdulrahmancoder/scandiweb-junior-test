<?php

use Core\Router;

class bootstrap
{
    public function __construct(){
        require 'core/ProductInterface.php';
        require 'core/Database.php';
        require 'models/Model.php';
        require 'core/abstractProduct.php';
        require 'core/router.php';
        require 'models/Product.php';
        require 'productTypes/Book.php';
        require 'productTypes/Furniture.php';
        require 'productTypes/DVD_desk.php';
        require 'controllers/ProductController.php';
        new Router();
    }

}
new bootstrap();